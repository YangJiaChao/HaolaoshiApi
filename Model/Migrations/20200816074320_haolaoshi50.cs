﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi50 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pic",
                table: "Post");

            migrationBuilder.AddColumn<string>(
                name: "Attachment",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Intro",
                table: "Post",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Attachment",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Intro",
                table: "Post");

            migrationBuilder.AddColumn<string>(
                name: "Pic",
                table: "Post",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
