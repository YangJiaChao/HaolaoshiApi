﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi18 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Emergency_number",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Education",
                table: "Teacher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Emergency_number",
                table: "Teacher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Graduate_school",
                table: "Teacher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Emergency_number",
                table: "Student",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Emergency_number",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Education",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Emergency_number",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Graduate_school",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Emergency_number",
                table: "Student");
        }
    }
}
