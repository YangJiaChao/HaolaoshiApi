﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi43 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "UsualScoreLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "UsualScoreItem",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "UsualScore",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "User",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "TimeTable",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Teacher",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Student",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Slider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Score",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "School",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "RoleAuthority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Role",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Res",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Post",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "OnlineResume",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Major",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Interview",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Images",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Group",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Grade",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "CourseRes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "CourseClass",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Company",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "College",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "ClockLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Clock",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "ClassSchedule",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Classs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Classroom",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Chapter",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "AuthorityRes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Authority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Article",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Area",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sys",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "School");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "CourseRes");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "CourseClass");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "College");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Chapter");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Area");
        }
    }
}
