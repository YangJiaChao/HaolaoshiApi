﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi49 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Interview");

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Company",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Post_TeacherId",
                table: "Post",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_TeacherId",
                table: "Company",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Post_TeacherId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Company_TeacherId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Company");

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Interview",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
