﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi31 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_Classs_ClasssId",
                table: "ClassSchedule");

            migrationBuilder.AlterColumn<string>(
                name: "CourseSn",
                table: "ClassSchedule",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "ClasssId",
                table: "ClassSchedule",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_Classs_ClasssId",
                table: "ClassSchedule",
                column: "ClasssId",
                principalTable: "Classs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_Classs_ClasssId",
                table: "ClassSchedule");

            migrationBuilder.AlterColumn<string>(
                name: "CourseSn",
                table: "ClassSchedule",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClasssId",
                table: "ClassSchedule",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_Classs_ClasssId",
                table: "ClassSchedule",
                column: "ClasssId",
                principalTable: "Classs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
