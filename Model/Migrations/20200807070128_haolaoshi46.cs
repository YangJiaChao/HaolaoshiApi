﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi46 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "UsualScoreLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "UsualScoreLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "UsualScoreItem",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "UsualScoreItem",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "UsualScore",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "UsualScore",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "User",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "User",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "TimeTable",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "TimeTable",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Teacher",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Teacher",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Student",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Student",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Slider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Slider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Score",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Score",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "School",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "School",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "RoleAuthority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "RoleAuthority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Role",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Role",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Res",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Res",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Post",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Post",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "OnlineResume",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "OnlineResume",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Major",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Major",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Interview",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Interview",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Images",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Images",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Group",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Group",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Grade",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Grade",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "CourseClass",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "CourseClass",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Company",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Company",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "College",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "College",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "ClockLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "ClockLog",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Clock",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Clock",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "ClassSchedule",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "ClassSchedule",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Classs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Classs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Classroom",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Classroom",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Chapter",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Chapter",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "AuthorityRes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "AuthorityRes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Authority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Authority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Article",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Article",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Area",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Passed",
                table: "Area",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Open",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "School");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "School");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "CourseClass");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "CourseClass");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "College");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "College");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Chapter");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Chapter");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "Passed",
                table: "Area");
        }
    }
}
