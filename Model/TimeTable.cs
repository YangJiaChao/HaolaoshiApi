﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    /// <summary>
    /// 作息时间，课时
    /// </summary>
    [Serializable]
    [Table("TimeTable")]
    public class TimeTable : SchoolUserID
    {
        /// <summary>
        /// 时间段编号
        /// </summary>
        [Required(ErrorMessage = "时间段编号必填")]
        public string Sn { get; set; }
        [Display(Name = "名称")]
        [Required(ErrorMessage = "时间段名称必填")]
        public string Name { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public TimeSpan  From_time { get; set; }
        /**
         * 截至时间
         */
        public TimeSpan To_time { get; set; }
    }
}
