using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class TimeTableBll : BaseBll<TimeTable>, ITimeTableBll
    {
        public TimeTableBll(ITimeTableDAL dal):base(dal)
        {
        }
    }
}
