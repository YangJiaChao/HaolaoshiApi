﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bll;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using Web.Filter;
using Web.Redis;
using Web.Security;

namespace Web.Controllers.Stu
{
    [Route("api/stu/")]
    [ApiController]
    [Authorize]
    [IdentityModelActionFilter]
    public class DefaultController : MyBaseController
    {
        public IClockLogBll clockLogBll { get; set; }
        // public IClaimsAccessor MyUser { get; set; }
        /// <summary>
        /// 签到打卡
        /// </summary>
        /// <param name="answer"></param>
        /// <returns>{Status:true,Code:200成功|500失败|501未开始|502已打卡,Msg:""}</returns>
        [HttpPost("clock")]
        //[HttpGet("clock/{answer}")]
        public Result Clock(string answer)
        {
            string classid = MyUser.Project;//班级id
            string name = MyUser.Name;
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.SetSysCustomKey("clock_stu");//已签到的学生集合     
            RedisValue expire = rd.StringGet(classid + "expire");//签到时长
            if (expire.IsNullOrEmpty)
            {
                return Result.Error("亲，签到未开始哦").SetCode(501);
            }
            if (!rd.SortedSetContains(classid, name))
            {//包含表示打卡过
                var rightanswer = rd.StringGet<string>(classid + "answer");//签到答案
                if (rightanswer != answer)
                {
                    return Result.Error("打卡失败，暗号错误");
                }
                rd.SortedSetAdd<string>(classid, name, DateTime.Now.ToOADate());
                ImHelper.SendChanMessage("student-" + MyUser.Id, "clockclass-" + classid, ImMsg.Msg("clockclass-" + classid, null, new Sender() { ClientId = "student-" + MyUser.Id }));
                return Result.Success("打卡成功");
            }
            return Result.Error("已打卡").SetCode(502);
        }
        /// <summary>
        /// 获取学生签到的教师id，即找谁签到 
        /// </summary>
        /// <returns></returns>
        [HttpPost("getclockteacher")]
        //[HttpGet()]
        public Result GetClockTeacher()
        {
            string classid = MyUser.Project;//班级id          
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.SetSysCustomKey("clock_stu");//已签到的学生集合      
            string teacherid = rd.StringGet(classid + "teacher");//签到时长
            return Result.Success().SetData(teacherid);
        }
        [HttpGet("clocklog")]
        public Result ClockLog([FromQuery] Dictionary<string, string> where)
        {
            // return Result.Success().SetData(clockLogBll.Query().
            // Where(o => o.StudentId == MyUser.Id).AsEnumerable().OrderByDescending(o=>o.Added_time));
            where.Add("StudentId", MyUser.Id.ToString());
            //where.Add("sort", "-Added_time");//排序
            var query = clockLogBll.Query(where);
            query.Items = query.Items.Select(o => new Model.ClockLog() { Added_time = o.Added_time, Clock = o.Clock, Status = o.Status, Student = o.Student });//{Added_time=o.Added_time.Date, Clock=o.Clock, Status=o.Status , Student =o.Student});
            return Result.Success("succeed").SetData(query);
        }
    }
}