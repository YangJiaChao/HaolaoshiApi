﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Filter;

namespace Web.Controllers.Stu
{
    /// <summary>
    /// 此接口遗弃，放到api目录下，即controller目录下
    /// </summary>
    [Route("api/stu/")]
    [ApiController] 
    public class CourseController : MyBaseController
    {
        ICourseBll bll;
        public IChapterBll chapterBll { get; set; }
        public CourseController(ICourseBll bll)
        {
            this.bll = bll;
        }
        [HttpGet("course/list")]
        public Result List([FromQuery] Dictionary<string, string> where)
        {           
            where.Add("Open", true.ToString());
            where.Add("Passed", true.ToString());
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Course/Get/5
        [HttpGet("course/{id}")]
        public Result Get(int id)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(id));
        }
        [HttpGet("chapter/list/{courseId}")]
        public Result ChapterList(int courseId)
        {
            return Result.Success("succeed").SetData(chapterBll.SelectAll(o => o.CourseId == courseId && o.ParentId == null));
        }

        // GET: api/Course/Get/5
        [HttpGet("chapter/{id}")]
        public Result ChapterGet(int id)
        {
            return Result.Success("succeed").SetData(chapterBll.SelectOne(id));
        }
    }
}
