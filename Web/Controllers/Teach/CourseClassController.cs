﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Filter;

namespace Web.Teach.Controllers
{

    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [IdentityModelActionFilter]
    public class CourseClassController : MyBaseController
    {
        public IClasssBll classsBll { get; set; }
        public ICourseBll courseBll { get; set; }
        ICourseClassBll bll;
        public CourseClassController(ICourseClassBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 查询课程对应的班级
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            var db = courseBll.DbContext();
            var linq = from a in db.Courses
                       join b in db.CourseClasss on a.Id equals b.CourseId into re
                       from r in re.DefaultIfEmpty()
                       where a.TeacherId == MyUser.Id && a.Sys == false
                       select new { Id = r.Id, CourseId = a.Id, Course = a, Classs = r.Classs, Teacher = r.Teacher };
            //教师对应课程           
            //where.Remove("sort");
            //where.Add("sort", "-Added_time,-Course");//排序
            return Result.Success("succeed").SetData(Pagination<Object>.Init(1, 1, linq.ToList()));
        }
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }

        // Get: api/CourseClass/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpGet("{id}")]
        public Result DeleteClasss([FromQuery] Dictionary<string, string> where)
        {

            return classsBll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result AddClasss(CourseClasss2 o)
        {
            o.Sys = false;
            o.TeacherId = MyUser.Id;
            classsBll.Add(o);
            var courseClass = new CourseClass() { Classs = o, CourseId = o.CourseId, TeacherId = MyUser.Id, Sys = false };
            return ModelState.IsValid ? (bll.Add(courseClass) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        [HttpPost]
        public Result UpdateClasss(CourseClasss2 o)
        {
            o.Sys = false;
            o.TeacherId = MyUser.Id;
            var c = classsBll.SelectOne(o.Id);
            if (c.Sys)
            {
                return Result.Error("修改失败,不能修改系统数据");
            }            
            return ModelState.IsValid ? (classsBll.Update(o) ? Result.Success("修改成功") : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
        /// <summary>
        /// 查询行政班级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Result QuerySysClass()
        {
            //where.Remove("sort");
            //where.Add("sort", "-Added_time,-Course");//排序
            return Result.Success("succeed").SetData(classsBll.SelectAll(o => o.Sys == true));
        }
        /// <summary>
        /// 导入班级
        /// </summary>
        /// <param name="classsId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpPost]
        public Result ImportClasss([FromForm]int classsId, [FromForm] int courseId)
        {
            var courseClass = new CourseClass() { ClasssId = classsId, CourseId = courseId, TeacherId = MyUser.Id, Sys = false };
            return ModelState.IsValid ? (bll.Add(courseClass) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
    }

    public class CourseClasss2 : Classs
    {
        public int CourseId { get; set; }
    }
}
