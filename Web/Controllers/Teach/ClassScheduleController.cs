﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Redis;
using Web.Filter;

namespace Web.Teach.Controllers
{

    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [IdentityModelActionFilter]
    public class ClassScheduleController : MyBaseController
    {
        IClassScheduleBll bll;
        public ClassScheduleController(IClassScheduleBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 获取今天的课程安排表
        /// </summary>
        /// <returns></returns>     
        [HttpPost]
        public Result Today()
        {
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.SetSysCustomKey("clock_stu");//已签到的学生集合     
            var rightanswer = rd.StringGet<string>(MyUser.Id+ "_teacher_answer");//签到答案           
            return Result.Success("succeed").SetData(new { clockanswer=rightanswer,schedule= bll.SelectAll(o => o.Date == DateTime.Now.Date && o.TeacherId == MyUser.Id) });
        }

    }
}
