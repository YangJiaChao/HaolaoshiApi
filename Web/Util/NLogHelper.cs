﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Util
{
    public class NLogHelper : NLog.Logger
    {      
        public static readonly NLog.Logger logger = NLog.Web.NLogBuilder.ConfigureNLog(ConfigHelper.Configuration["Logging:NLog:Path"] + "\\nlog.config").GetCurrentClassLogger();
    }
}
